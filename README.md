# staging

GitLab OpenTelemetry Observability for staging environment.

## References

- https://docs.gitlab.com/ee/development/stage_group_observability/gitlab_instrumentation_for_opentelemetry.html
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148695+
